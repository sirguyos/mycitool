# Pull or Clone a repository using git-python
# GitPython requires git being installed on the system, and accessible via system's PATH.
import os, time, git
      
class Repo:
    def __init__(self, repo_url):
        self.repo_url = repo_url
        self.repos_dir = os.path.dirname(os.path.realpath(__file__))+'/repos/'
        self.repo_name = repo_url.rsplit('/')[-1].split('.')[0]
        self.repo_full_name = repo_url.rsplit('/')[-2]+'/'+self.repo_name
        self.path = self.repos_dir+self.repo_name

    def getName(self):
        return self.repo_name
    
    def getFullName(self):
        return self.repo_full_name

    # full path
    def getPath(self):
        return self.path

    def get(self):
        if os.path.exists(self.path):
            self.pull(self.path)
        else:
            self.clone()

    def clone(self):
        print('Cloning', self.repo_name)
        git.Repo.clone_from(self.repo_url, self.repos_dir+self.repo_name, progress=MyProgressPrinter())
        print('100%')
        print('Done.')
    
    def pull(self, path):
        repo = git.Repo(path)
        origin = repo.remotes.origin
        print('Pulling', self.repo_name)
        try:
            origin.pull(progress=MyProgressPrinter())
        except ValueError:
            print("Error pulling the repo - delete the directory", path, 'and run again')
            exit(ValueError)
        print('100%')
        print('Done.')

# progress printer for git-python
class MyProgressPrinter(git.RemoteProgress):
    def update(self, op_code, cur_count, max_count=None, message=''):
        print(str(round(cur_count / (max_count or 100.0) * 100))+'%', end='\r')
        time.sleep(0.1)