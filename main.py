
from pprint import pprint
import sys

# Project Classes
from repo import Repo
from container import Container

# hash: repo_full_name => repo_url
repos_full_name = {}

# this function will clone/pull a repo
# build an image from dockerfile and run a container
def runRepo(repo):
    if repo[0] != '#':
        r = Repo(repo)
        r.get()
        repos_full_name[r.getFullName()]=repo
        c = Container(r.getName(), r.getPath())
        c.build()
        c.run()

# get repos from file
if sys.argv[1] == '-f':
    file = sys.argv[2]
    with open(file) as f:
        repos = f.readlines()
    repos = [x.strip() for x in repos]
# get one repo as an argv
else:
    repos = [sys.argv[1]]

for repo in repos:
    runRepo(repo)

# "agent" listens to webhooks
from flask import Flask, request, jsonify
import json
app = Flask(__name__)
@app.route('/webhook',methods=['POST'])
def hook():
   data = json.loads(request.data)
   repo_full_name = data['repository']['full_name']
   print(repo_full_name)
   print(repos_full_name)
   if repo_full_name in repos_full_name:
       print(repos_full_name[repo_full_name])
       runRepo(repos_full_name[repo_full_name])
   return jsonify(
        id='1'
    )

if __name__ == '__main__':
    try:
        app.run(host='0.0.0.0', port=8081)
    except Exception:
        print(Exception)