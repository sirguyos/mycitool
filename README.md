# MyCITool
Will clone/pull repository and will attempt to create a docker machine from it.

Upon recieving a webhook, mycitool will pull the code again and restart the container.

## Prerequisites:
* git command in path
* git credentials to the repository
* docker daemon running
* Dockerfile present in the repository's root directory.
    * if you need ports exposed make sure you specify them inside your dockerfile (ex. EXPOSE 80), mycitool will take care of the rest.
* MyCITool is listening for <b>POST</b> webhooks on port <b>tcp/8081</b>, you can use that for continuos integration by setting a webhook on your git repository. ex: <b>http://<machine_running_mycitool_ip>:8081/webhook</b>

## How to run:
1. Run inside mycitool folder: <code>python -m venv venv</code>
2. Activate the virtual env
    * Windows: <code>venv\Scripts\activate</code>
    * Linux / OSX: <code>source venv/bin/activate</code>
3. Run: <code>pip install -r requirements.txt</code>
4. Run the program:
    * For one repo: <code>main.py \<url to get repo></code>
        * For example: main.py https://github.com/ssages/python-cherry-container.git
    * For multiple repos, use a file: <code>main.py -f repos.txt</code>
        * File should container repositories' url line by line, see repos.txt for an example.
5. You can add test commands to the file test.txt to test your container's functionality:
    * ex {'repo_name' : ['echo hi', 'curl http://localhost:80']}