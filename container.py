import docker, time, json, os, subprocess, re

class Container:
    def __init__(self, tag, path):
        self.tag = tag
        self.path = path
        self.client = docker.from_env()
        self.image_tag = None

    def setImageTag(self, image_tag):
        self.image_tag = image_tag
    
    # build's image and prepare run command to run the image
    def build(self, **kwargs):
        print('\nAttempting to build container image.')
        tag = kwargs.get('tag', None)
        path = kwargs.get('path', None)
        if not tag:
            tag = self.tag
        if not path:
            path = self.path
        try:
            dimage = self.client.images.build(path=path, tag=tag, rm=True, labels={'mct-'+self.tag:'test'})
            self.setImageTag(dimage[0].tags[0])
            print('Image built successfully.')
        except Exception as e:
            print('Error while building:', self.tag)
            print(e)
            exit(1)

    def getPorts(self):
        portsDict = {}
        with open(self.path+'/Dockerfile') as dockerfile:
            r = re.search(r"EXPOSE\s(\d+\/?\w*\s?)+", dockerfile.read())
            ports = r[0].split(" ")[1:]
            for port in ports:
                rp = re.search(r"(\d+)\/?\w*", port.rstrip())
                portsDict[rp[0]] = rp[1]
        return portsDict

    # run a container
    def run(self):
        if self.image_tag:
            self.cleanContainers()
            c = self.client.containers.run(self.image_tag, detach=True, ports=self.getPorts())
            print('\nContainer was',c.status)
            print('Waiting for container to start.')
            dot = '.'
            while c.status == 'created':
                time.sleep(1)
                print(dot, end='\r')
                dot += dot
                c = self.client.containers.get(c.id)
            c = self.client.containers.get(c.id)
            print('Container is',c.status)
            if c.status=='running':
                self.test(c)
            else:
                print('Container', self.image_tag, 'is not running. Exiting.')
                exit(1)
        else:
            print('No image tag set, try setting one by using setImageTag or by building a new image')
    
    def test(self, container):
        repos = json.loads(open('test.json').read())
        if self.tag in repos:
            print('\nRunning tests.')
            error = False
            for command in repos[self.tag]:
                print('\nRunning command:',command)
                command = command.split(' ')
                s = subprocess.run(command, shell=True)
                if s.returncode != 0:
                    error = True
            if error:
                print('\nSome tests failed. Exiting.')
                exit(1)
            print('\n\nFinished tests.\n')

    # clean docker containers by repo label
    def cleanContainers(self):
        dcontainers = self.client.containers.list(all=True, filters={'label':'mct-'+self.tag})
        for container in dcontainers:
            container.stop()
            container.remove()